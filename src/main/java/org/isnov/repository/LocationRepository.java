/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.isnov.repository;




import java.math.BigDecimal;
import java.util.List;
import org.isnov.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author isnov-pc
 */
public interface LocationRepository extends JpaRepository<Location, Integer> {
        @Modifying
    
    @Query(value ="update adempiere.c_location  set Latitude= ?3 , Longitude= ?2  where c_location_id = ?1 \n" 
 , nativeQuery = true)
@Transactional   
    public void updateBPartner( Integer order_id,  BigDecimal longitude , BigDecimal latitude);
    
    
        @Modifying
    
    @Query(value ="update adempiere.c_order  set Latitude= ?3 , Longitude= ?2  where c_order_id = ?1 \n" 
 , nativeQuery = true)
@Transactional   
    public void updateOrder( Integer order_id,  BigDecimal longitude , BigDecimal latitude);
       
                                           
    
    
    
    
    
}
