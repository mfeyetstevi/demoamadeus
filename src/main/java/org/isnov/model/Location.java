/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.isnov.model;

import java.io.Serializable;
import java.security.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author isnov-tech-04
 */
@Entity
@Table(name = "C_Location")
public class Location implements Serializable  {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "C_Location_ID")
    private Integer C_Location_ID;
    
    @Basic(optional = false)
     @Column(name = "ad_client_id")
    private String ad_client_id;
    
     @Basic(optional = false)
      @Column(name = "ad_org_id")
    private String ad_org_id;
     
      @Basic(optional = false)
            @Column(name = "isactive")
    private String isactive;
    @Basic(optional = false)        
             @Column(name = "created")
    private Timestamp created;
    
    @Basic(optional = false)
                    @Column(name = "createdby")
    private Integer createdby;
                    
    @Basic(optional = false)
                     @Column(name = "updated")
    private Timestamp updated;
    @Basic(optional = false)
                     @Column(name = "updatedby")
    private Timestamp updatedby;
    @Basic(optional = false)
                     @Column(name = "c_country_id")
                     private Integer C_Country_ID;
                     

                      @Column(name = "c_order_id")
                     private Integer c_order_id;
               
   
                      @Column(name = "latitude")
                     private String Latitude;
    
                     
                      @Column(name = "longitude")
                     private String Longitude;
     
          @Basic(optional = false)            
                       @Column(name = "c_location_uu")
                     private String c_location_uu;

    public Integer getC_Location_ID() {
        return C_Location_ID;
    }

    public void setC_Location_ID(Integer C_Location_ID) {
        this.C_Location_ID = C_Location_ID;
    }

    public String getAd_client_id() {
        return ad_client_id;
    }

    public void setAd_client_id(String ad_client_id) {
        this.ad_client_id = ad_client_id;
    }

    public String getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(String ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Timestamp getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(Timestamp updatedby) {
        this.updatedby = updatedby;
    }

    public Integer getC_Country_ID() {
        return C_Country_ID;
    }

    public void setC_Country_ID(Integer C_Country_ID) {
        this.C_Country_ID = C_Country_ID;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    public String getC_location_uu() {
        return c_location_uu;
    }

    public void setC_location_uu(String c_location_uu) {
        this.c_location_uu = c_location_uu;
    }
                      
                     
}
