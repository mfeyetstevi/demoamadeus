/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.isnov.model;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author isnov-tech-04
 */

public class BPGeolocalisation {
   
   private BigDecimal longitude;
   private BigDecimal Latitute;
   private Integer c_order_id;

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitute() {
        return Latitute;
    }

    public void setLatitute(BigDecimal Latitute) {
        this.Latitute = Latitute;
    }

 

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }
   
   
}
