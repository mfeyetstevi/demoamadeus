/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isnov.webservice.demo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.coyote.Response;
import org.isnov.model.BPGeolocalisation;
import org.isnov.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.amadeus.Amadeus;
import com.amadeus.Params;

import com.amadeus.exceptions.ResponseException;
import com.amadeus.referenceData.Locations;
import com.amadeus.resources.Airline;
import com.amadeus.resources.FlightDestination;
import com.amadeus.resources.HotelOffer;
import com.amadeus.resources.Location;
import java.util.ArrayList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isnov-tech-04
 */
@RestController
public class SpringJava4sController {

    @Autowired
    private LocationRepository bp;

    @RequestMapping(path = "/list", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Object> crunchifyREST(@RequestBody String incomingData) {
        StringBuilder crunchifyBuilder = new StringBuilder();

        System.out.println("Data Received: " + incomingData);

        // return HTTP response 200 in case of success
        return ResponseEntity.status(200).body(incomingData);
    }
    
@CrossOrigin(origins="*")
@PostMapping(path = "/send", consumes = "application/json", produces = "application/json")

    public BPGeolocalisation addEmployee(@RequestBody BPGeolocalisation employee)
            throws Exception {
        //Generate resource id
        bp.updateOrder(employee.getC_order_id(),employee.getLongitude(),employee.getLatitute());
BPGeolocalisation b=new BPGeolocalisation();
        //Send location in response
        return b;
    } 
    
    @CrossOrigin(origins="*")
@PostMapping(path ="/sendbpl", consumes = "application/json", produces = "application/json")

    public BPGeolocalisation addEmployeeBPL(@RequestBody BPGeolocalisation employee)
            throws Exception {
        //Generate resource id
        bp.updateBPartner(employee.getC_order_id(),employee.getLongitude(),employee.getLatitute());
BPGeolocalisation b=new BPGeolocalisation();
        //Send location in response
        return b;
    } 
    
    
    
    
    Amadeus amadeus = Amadeus
        .builder("P2YBubto3qFx5KQ1ZqqgMqvmPZj04DSy", "C2GMHWbiX4iNJgWY")
        .build();
     @CrossOrigin(origins="*")
@PostMapping(path =  "/client/listvilecompagnie/{keyword}/{subtype}", consumes = "application/json", produces = "application/json")

    public ArrayList<Location> addEmployeeBPL(@PathVariable("keyword") String airlineCodes,@PathVariable("subtype") String subtype)
            throws Exception {
     Location[] airlines = amadeus.referenceData.locations.get(Params
  .with("keyword", airlineCodes)
  .and("subType", subtype));
     
     ArrayList<Location>  listairlines=new ArrayList<Location> ();
     for(Location f: airlines){
         listairlines.add(f);
     }
        //Send location in response
        return listairlines;
    }    
         @CrossOrigin(origins="*")
@GetMapping(path =  "/client/listhotelbycitycode/{cityCode}", consumes = "application/json", produces = "application/json")

    public HotelOffer[] listhotelbycitycode(@PathVariable("cityCode") String cityCode)
            throws Exception {
        HotelOffer[] offers = amadeus.shopping.hotelOffers.get(Params
  .with("cityCode", cityCode));
        return offers;
        
    }
             @CrossOrigin(origins="*")
@GetMapping(path =  "/client/listhotelbyidhotel/{hotelId}", consumes = "application/json", produces = "application/json")

    public HotelOffer listhotelbyhotelcode(@PathVariable("hotelId") String hotelId)
            throws Exception {
       HotelOffer hotelOffer = amadeus.shopping.hotelOffersByHotel.get(Params.with("hotelId", hotelId));
        return hotelOffer;
        
    }
    
}
